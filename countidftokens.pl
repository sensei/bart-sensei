#!/usr/bin/perl -w

my %tokens=();
my %curtokens=();
my $thr=5;


while(<>) {
chomp;
my $cur=$_;
if ($cur=~/^\s*====/) {
  foreach my $k (keys %curtokens) {
    $tokens{$k}++;
  }
  %curtokens=();
}
next if not $cur=~/^\s*NEXTNEXTT\s+(.+?)\s*$/;
$curtokens{$1}=1;
}
foreach my $k (keys %curtokens) {
  $tokens{$k}++;
}

foreach my $k (sort keys %tokens) {
next if $tokens{$k}<$thr;
print "$k\n";
}
