#!/usr/bin/perl -w

## Take the output of CONLL scorer and make it more readable

my %r=();
my %p=();
my %f=();
my $metr="";
my @evals=("emd","muc","bcub","ceafe","ceafm");

sub dump_hashes {
  return 0 if not defined $r{"emd"};
  foreach (@evals) {
   print "$_: R=".$r{$_}." P=".$p{$_}." F=".$f{$_}."\n";
  }

  my $final=($f{"muc"}+$f{"ceafe"}+$f{"bcub"})*0.33;

  print "Final score: $final\n";

}
while(<>) {

chomp;
my $cur=$_;

if ($cur=~/=====/) {
  if (not $cur=~/TOTAL/) {
    dump_hashes;
    print $cur."\n";
    print " -- BART scores: --\n";
    %r=();
    %p=();
    %f=();
  }
  next;
}

if ($cur=~/SCORER/) {
  print $cur."\n";
  next;
}

if ($cur=~/Identification of Mentions: Recall: .+ (\d+.\d*)\%	Precision: .+ (\d+.\d*)\%	F1: (\d+.\d*)\%/) {
  $r{"emd"}=$1;
  $p{"emd"}=$2;
  $f{"emd"}=$3;
  next;
}
if ($cur=~/^METRIC (.+):/) {
$metr=$1;
next;
}

if ($cur=~/Coreference: Recall: .+ (\d+.\d*)\%	Precision: .+ (\d+.\d*)\%	F1: (\d+.\d*)\%/) {
  $r{$metr}=$1;
  $p{$metr}=$2;
  $f{$metr}=$3;
  next;
}
}

dump_hashes;
