#!/usr/bin/perl -w

my %tokens=();
my $thr=5;
while(<>) {
chomp;
next if not /^\s*NEXTNEXTT\s+(.+?)\s*$/;
$tokens{$1}++;
}

foreach my $k (sort keys %tokens) {
next if $tokens{$k}<$thr;
print "$k\n";
}
