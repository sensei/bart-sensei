/************************************************************************/
/*                                                                      */
/*   kernel.h                                                           */
/*                                                                      */
/*   User defined kernel function. Feel free to plug in your own.       */
/*                                                                      */
/*   Copyright: Alessandro Moschitti                                    */
/*   Date: 20.11.06                                                     */
/*                                                                      */
/************************************************************************/

/* KERNEL_PARM is defined in svm_common.h The field 'custom' is reserved for */
/* parameters of the user defined kernel. */
/* Here is an example of custom kernel on a forest and vectors*/                          


double custom_kernel(KERNEL_PARM *kernel_parm, DOC *a, DOC *b) 
{

  int i;
  double k=0, Poly=0, TK0=0, TK1=0, TK2=0, SK3=0, SK4=0, SK5=0, SK6=0, SK7=0, SK1=0;
  

 // kernel_parm->first_kernel=6;

  
 //    if(a->num_of_trees == 0 || b->num_of_trees == 0) return 0;
//     else   
//       if(a->num_of_trees <=8 || b->num_of_trees<=8){
//         printf("\nERROR: attempting to access to a tree not defined in the data\n\n");
//          fflush(stdout);
//         exit(-1);
//       }
//       else if(a->forest_vec[7]->root==NULL || b->forest_vec[7]->root==NULL) return 0;
//i=7;
  //  printf("\n\n\n nodes: %d  %d\n", a->forest_vec[i]->root->nodeID,b->forest_vec[i]->root->nodeID);
//    printf("node list lenghts: %d  %d\n", a->forest_vec[i]->listSize,b->forest_vec[i]->listSize);
//    printf("doc IDs :%ld %ld",a->docnum,b->docnum);fflush(stdout);
//    printf("\ntree 1: "); writeTreeString(a->forest_vec[i]->root);fflush(stdout);
//    printf("\ntree 2: "); writeTreeString(b->forest_vec[i]->root);fflush(stdout);
//

//kernel_parm->first_kernel=6;
//  if(tree_kernel(kernel_parm, a, a, 7, 8)>0.9) SK7=1; else SK7=0;
//  if(tree_kernel(kernel_parm, b, b, 7, 8)>0.9) SK8=1; else SK8=0;


//   SK3=tree_kernel(kernel_parm, a, a, 3, 4);SK4=tree_kernel(kernel_parm, b, b, 3, 4);
//   SK5=tree_kernel(kernel_parm, a, a, 5, 6);SK6=tree_kernel(kernel_parm, b, b, 5, 6);

//    printf("Kernel :%0.20f norm %f\n",SK78,sqrt(a->forest_vec[i]->twonorm_PT * b->forest_vec[i]->twonorm_PT));

  // SK5=tree_kernel(kernel_parm, a, b, 5, 5)+tree_kernel(kernel_parm, a, b, 6, 6)+

   kernel_parm->first_kernel=6;


//  SK3= tree_kernel(kernel_parm, a, b, 3, 3)+tree_kernel(kernel_parm, a, b, 4, 4);
  SK5= tree_kernel(kernel_parm, a, b, 5, 5)+tree_kernel(kernel_parm, a, b, 6, 6);
  SK7= tree_kernel(kernel_parm, a, b, 7, 7)+tree_kernel(kernel_parm, a, b, 8, 8);
  
   kernel_parm->first_kernel=1;
   TK0 = tree_kernel(kernel_parm, a, b, 0, 0); // Evaluate tree kernel between the two i-th trees.
   TK1 = tree_kernel(kernel_parm, a, b, 1, 1)+tree_kernel(kernel_parm, a, b, 2, 2);   
   Poly = basic_kernel(kernel_parm, a, b, 0, 0); // Compute standard kernel (selected according to the "second_kernel" parameter).

//if (SK78>0) printf("Kernel :%0.20f norm %f\n",SK78,sqrt(a->forest_vec[i]->twonorm_PT * b->forest_vec[i]->twonorm_PT));
 //if((k=TK1*TK2+SK3*SK4+SK5*SK6+SK7*SK8)>0) k=k/sqrt((TK1*TK1+SK3*SK3+SK5*SK5+SK7*SK7)*(TK2*TK2+SK4*SK4+SK6*SK6+SK8*SK8));

   return (SK3+SK5+SK7+TK0+TK1+1)*Poly; //SK3+(SK1+TK0)*Poly+Poly;
}




/*BEST KERNEL

  kernel_parm->first_kernel=6;
  //SK3= tree_kernel(kernel_parm, a, b, 3, 3)+tree_kernel(kernel_parm, a, b, 4, 4);
  SK5= tree_kernel(kernel_parm, a, b, 5, 5)+tree_kernel(kernel_parm, a, b, 6, 6);
  
   kernel_parm->first_kernel=1;
   TK0  = tree_kernel(kernel_parm, a, b, 0, 0); // Evaluate tree kernel between the two i-th trees.
   kernel_parm->first_kernel=3;
   TK1=tree_kernel(kernel_parm, a, b, 1, 1)+tree_kernel(kernel_parm, a, b, 2, 2);
   
   Poly = basic_kernel(kernel_parm, a, b, 0, 0); // Compute standard kernel (selected according to the "second_kernel" parameter).
*/
