/************************************************************************/
/*                                                                      */
/*   kernel.h                                                           */
/*                                                                      */
/*   User defined kernel function. Feel free to plug in your own.       */
/*                                                                      */
/*   Copyright: Alessandro Moschitti                                    */
/*   Date: 20.11.06                                                     */
/*                                                                      */
/************************************************************************/

/* KERNEL_PARM is defined in svm_common.h The field 'custom' is reserved for */
/* parameters of the user defined kernel. */
/* Here is an example of custom kernel on a forest and vectors*/                          


double custom_kernel(KERNEL_PARM *kernel_parm, DOC *a, DOC *b) 
{

  int i;
  double Poly=0, TK0=0;
  
i=0;    
       TK0  = tree_kernel(kernel_parm, a, b, atoi(kernel_parm->custom), atoi(kernel_parm->custom)); // Evaluate tree kernel between the two i-th trees.

       Poly = basic_kernel(kernel_parm, a, b, i, i); // Compute standard kernel (selected according to the "second_kernel" parameter).

   return Poly +TK0*kernel_parm->tree_constant; // best k1*k2+k2; // 
}
