#!/usr/bin/perl -w

my %fs=();

my $fid=0;

my $fdata=$ARGV[0];
my $flist=$ARGV[1];

open(IN,$fdata);
while(<IN>) {
  $fid++ if /w/;
  next if $fid==0;
  next if /w/;
  chomp;
  my $cur=$_;
  $fs{$fid}=$cur if not $cur==0;
  $fid++;
}
close IN;

my %fnames=();

open(IN,$flist);
while(<IN>){
  chomp;
  next if not /^\s*(\d+)\s*=> :(.+)/;
  $fnames{$1}=$2;
}
foreach my $k (sort{ $fs{$b}*$fs{$b} <=> $fs{$a}*$fs{$a} } keys %fs) {
  my $pos="";
  $pos="POSITIVE " if $fs{$k}>0;
  print $pos.$k." ".$fs{$k}." ".$fnames{$k}."\n";
}
