#!/usr/bin/perl -w

my $bartdir="/home/olga/hie/BART-2.0";
my $scriptdir="$bartdir/tests";
my $javacmd="/tools/jdk1.7.0_21/bin/java";
my $conlldatadir="/mnt/sdc/data/olga/conll2014/data/";
my $bartdatadir=$conlldatarid."mmax/";

##my $bartdir="/Users/oliunya/BART2.0/bartcomb/BART-2.0";
##my $scriptdir="$bartdir/tests";



my $corpus="ontoenglish2014";

my $annoonly=0;
my $nocollect=0;
my $nochi=1;

use Testing::Report;
Testing::Report::set_report($scriptdir);
my $repfile=Testing::Report::get_report_name("liblinear-brazil-fcomb-tunechi-tuneprc");

my @xmls=(
#	  "presets/soonbaseline.xml",		
#	  "presets/conll_submitted_nosplit_binary_svm.xml",		
#	  "presets/soonbaseline_binary.xml",
#	  "presets/soonbaseline_svm.xml",
#	  "presets/soonbaseline_binary_svm.xml",	
#	  "presets/soonbaseline_svmd2.xml",
#	  "presets/soonbaseline_binary_svmd2.xml",

          "brazilbinarybart.sievemlhie.ll.xml",         
#          "brazilbinarybart.sieveml.ll.xml",                
	 );

my @misc=(
#"$bartdir/models/coref/ftempl/jim.c15.pg1.ng0.templ",
#"$bartdir/models/coref/ftempl/jim.c15.pg1.ng1.templ",
"$bartdir/models/coref/ftempl/jim.c15.pg10.ng0.templ",
"$bartdir/models/coref/ftempl/jim.c15.pg10.ng1.templ",

#"$bartdir/models/coref/ftempl/jim.c5.pg1.ng0.templ",
#"$bartdir/models/coref/ftempl/jim.c5.pg1.ng1.templ",
"$bartdir/models/coref/ftempl/jim.c5.pg10.ng0.templ",
"$bartdir/models/coref/ftempl/jim.c5.pg10.ng1.templ",

"$bartdir/models/coref/ftempl/jim.c10.pg1.ng0.templ",
#"$bartdir/models/coref/ftempl/jim.c10.pg1.ng1.templ",
"$bartdir/models/coref/ftempl/jim.c10.pg10.ng0.templ",
#"$bartdir/models/coref/ftempl/jim.c10.pg10.ng1.templ",


"$bartdir/models/coref/ftempl/jim.c20.pg1.ng0.templ",
#"$bartdir/models/coref/ftempl/jim.c20.pg1.ng1.templ",
"$bartdir/models/coref/ftempl/jim.c20.pg10.ng0.templ",
#"$bartdir/models/coref/ftempl/jim.c20.pg10.ng1.templ",


#"$bartdir/models/coref/ftempl/jim.c5.pg1.ng10.templ",
#"$bartdir/models/coref/ftempl/jim.c5.pg10.ng10.templ",
#"$bartdir/models/coref/ftempl/jim.c10.pg1.ng10.templ",
#"$bartdir/models/coref/ftempl/jim.c10.pg10.ng10.templ",
#"$bartdir/models/coref/ftempl/jim.c15.pg1.ng10.templ",
#"$bartdir/models/coref/ftempl/jim.c15.pg10.ng10.templ",
#"$bartdir/models/coref/ftempl/jim.c20.pg1.ng10.templ",
#"$bartdir/models/coref/ftempl/jim.c20.pg10.ng10.templ",
);

my @cparams=(##0.01,100); ##
1,#0.3,3,0.1,1
);
my @bparams=(1,#0
);
my @prcparams=(
#1,
2,3,4,
#5,
##7,10
);

my @tkparams=(#
#-1,
0,
#1,
#2,
#3
); ##0 -- for DT-based features
#my @pruneparams=(10,
#100,
#1000,
#1); ## remove features that are too rare

my @chiparams=(
#100,
#2000,
5000,

#6000,
#7000,
#1000,
#4000,
10000,
#10000,
#20000,
#100000,
#200000,
); ## max number of features to be kept by the chi-prefiltering
my @combparams=(5); ##10,5,3,2,20);

sub  adjust_config{
  my ($b,$c,$tk,$exp,$prc)=@_;
  open(IN,"$bartdir/$exp.temp")||die "Cannot open $bartdir/$exp: $!\n";
  open(OUT,">$bartdir/$exp")||die "Cannot open $bartdir/$exp: $!\n";
  foreach my $l (<IN>) {
    $l=~s/options=""/options="-c $c -k $tk -b $b -p $prc"/;
    print OUT $l;
  }
  close IN;
  close OUT;
}




###### conll - english ######

system("echo ' ======== conll-english ========== ' >> $repfile");
system("echo ' --> bart and brazil features, olia sieves, yes contexttoken, yes nonana pairs, jim tm templated, normalized by fvec length, no chi filtering ' >> $repfile");

foreach my $templ (@misc) {
        system("echo ' =============== $templ ========== ' >>$repfile");


	system("cp $templ $bartdir/models/coref/idc0.fcomb.all");
foreach my $tk (@tkparams) {
  my @newcomb=@combparams;
  @newcomb=(1) if not $tk==0; 
  foreach my $maxcomb (@newcomb) {
  foreach my $exp (@xmls) {
system("$scriptdir/remove2bigcomb.pl $maxcomb $bartdir") if $tk==0;
	adjust_config(0,0,$tk,$exp,1);
if ($nocollect==0) {
system("(cd $bartdir ; $javacmd -Xmx4000m -Delkfed.corpus=$corpus elkfed.main.XMLTrainer $bartdir/$exp) ") if $annoonly==0;
system("cp $bartdir/models/coref/idc0.data $bartdir/models/coref/idc0.data.all") if $annoonly==0; 
}
#foreach my $prunep (@pruneparams) {
foreach my $chip (@chiparams) {
  my $prunep=$chip;
  ##my $prunep=$chip*$tk if $tk>0;
  #system("$scriptdir/remove_rarefs.pl $prunep $bartdir");
  #if ($nocollect==0) {
  if ($nochi==0) {
    system("$scriptdir/remove_chi.pl $prunep $bartdir")  if $annoonly==0;
    #die;
    #}
  }
    foreach my $b (@bparams) {

      foreach my $c (@cparams) {

      foreach my $prc (@prcparams) {

	adjust_config($b,$c,$tk,$exp,$prc);
	system("echo ' --> $exp maxcomb=$maxcomb k=$tk p=$prunep b=$b c=$c prc=$prc' >> $repfile");


system("(cd $bartdir ; $javacmd -Xmx20000m -Delkfed.corpus=$corpus elkfed.main.XMLClassifierBuilder $bartdir/$exp) ") if $annoonly==0;
        system("echo ' =============== development ========== ' >>$repfile");

	system("(cd $bartdir ;  $javacmd -Xmx3000m -Delkfed.corpus=$corpus elkfed.main.XMLAnnotator $bartdir/$exp) > log_liblinear_k${tk}_m${maxcomb}_p{$prunep}_b${b}_c${c}.log");
	system(" cat log_liblinear_k${tk}_m${maxcomb}_p{$prunep}_b${b}_c${c}.log |grep TOTAL >> $repfile");

        system("echo 'official v4 score on dev: ' >>$repfile");
        system("$scriptdir/prepare4conllscorer2014.pl $conlldatadir/mmax/dev $conlldatadir/conll-2012/v4/data/development/data/english/ |$scriptdir/interpret_scores.pl >>$repfile");
        system("echo ' =============== test ========== ' >>$repfile");

        system("(cd $bartdir ;  java -Xmx3000m -Delkfed.corpus=${corpus}t elkfed.main.XMLAnnotator $bartdir/$exp) ");

        system("echo 'official v4 score on test: ' >>$repfile");
        system("$scriptdir/prepare4conllscorer2014test.pl $conlldatadir/mmax/test $conlldatadir/conll-2012/v4/data/test/data/english/ |$scriptdir/interpret_scores.pl >>$repfile");
        system("echo 'official v7 score on test: ' >>$repfile");
        system("$scriptdir/prepare4conllscorer2014test-v7.pl $conlldatadir/mmax/test $conlldatadir/conll-2012/v4/data/test/data/english/ |$scriptdir/interpret_scores.pl >>$repfile");
      }
    }
    }
  last if $nochi==1;
}
}}
}}
