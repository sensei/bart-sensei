#!/usr/bin/perl -w

while(<>) {
  chomp;
  my $cur=$_;
  if ($cur=~/^\d/) {
    my @vals=split(",",$cur);
    $vals[0]="NONE";
    $vals[1]="NONE";
    $vals[2]="NONE";
    $vals[3]="NONE";
    $vals[4]="NONE";
    $cur=join(",",@vals);
  }
  print $cur."\n";
}
